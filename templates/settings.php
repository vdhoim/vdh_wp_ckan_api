<div class="wrap">
    <h2>Settings</h2>
    <?php if (isset($error) && $error): ?>
        <div id="message" class="error below-h2">
            <p><?php echo $error['msg'] ?></p>
        </div>
    <?php endif; ?>
    <?php $active = !isset($error['tab']); ?>
    <h2 class="nav-tab-wrapper">
        <?php foreach (array_keys($data) as $tab_name): ?>
            <a href="#<?php echo $tab_name; ?>" class="nav-tab<?php echo (isset($error['tab']) && $tab_name === $error['tab'] ? ' nav-tab-active': '')?>">               
                <?php echo implode(' ', array_map('ucfirst', explode('_', $tab_name))); ?>
            </a>
        <?php endforeach; ?>
    </h2>
    <?php foreach ($data as $obj_name => $obj): ?>
        <?php if (isset($error['tab']) && $obj_name === $error['tab']): ?>
            <?php $active = true; ?>
        <?php endif; ?>
        <div id="<?php echo $obj_name; ?>" class="tab-pane<?php echo ($active ? ' active' : ''); ?>">
            <div>
                <?php Ckan_Api_Functions::add_template("partials/$obj_name", array('data' => $obj)); ?>
            </div>
        </div>
        <?php $active = false; ?>
    <?php endforeach; ?>
</div>