<div class="wrap">
    <h2 class="nav-tab-wrapper">
        <?php foreach ($data as $obj_name => $obj): ?>
            <a href="#<?php echo $obj_name; ?>" class="nav-tab<?php echo ($obj['active'] ? ' nav-tab-active' : ''); ?>">
                <?php echo ucfirst($obj_name); ?>
            </a>
        <?php endforeach; ?>
    </h2>
    <?php foreach ($data as $obj_name => $obj): ?>
    
        <div id="<?php echo $obj_name; ?>" class="tab-pane<?php echo ($obj['active'] ? ' active' : ''); ?>">
            <div>
                <button class="button button-primary add_new <?php echo $obj_name ?>">
                    Add New <?php echo substr(ucfirst($obj_name), 0, -1) ?>
                </button>
                <?php if($obj_name === 'datasets'): ?>
                    <?php Ckan_Api_Functions::add_template("partials/snippets/search", array('object' => $obj_name)); ?>
                <?php endif; ?>
                
                <?php if (is_array($obj)): ?>
                        <?php Ckan_Api_Functions::add_template("partials/$obj_name", array('data' => $obj)); ?>
                <?php else: ?>
                    <?php Ckan_Api_Functions::add_template("partials/error", array('data' => $obj)); ?>
                <?php endif; ?>
                
                <button class="button button-primary add_new <?php echo $obj_name ?>">
                    Add New <?php echo substr(ucfirst($obj_name), 0, -1) ?>
                </button>
                
                <?php if($obj_name === 'datasets'): ?>
                    <?php
                    Ckan_Api_Functions::add_template("partials/snippets/pagination", array(
                        'page_count' => ceil( (int) (count($obj[$obj_name . '_list']) / (get_option('datasets_per_page') ? (int) get_option('datasets_per_page') : 10)) ),
                        'object' => $obj_name));
                    ?>
                <?php endif; ?>
        <?php Ckan_Api_Functions::add_template("add_$obj_name", array('data' => $obj)); ?>
            </div>
        </div>
<?php endforeach; ?>
</div>
<div class="overlay"></div>
<script type="text/html" id="tmpl-fileinfo">
<?php Ckan_Api_Functions::add_template('partials/snippets/resource'); ?>
</script>