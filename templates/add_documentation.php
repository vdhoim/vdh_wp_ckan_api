<div class="add_new_form documentation">
    <h2>Add Documentation</h2>
    <h3>The documentation should be zipped into .zip archive and the archive root should contain index.html file</h3>
    <table class="form-table">
        <tbody>
            <tr>
                <th>
                    <label for="title">Documentation Name</label>
                </th>
                <td>
                    <input id="title" name="title" type="text" />
                    <span class="description">Description</span>
                </td>
            </tr>
            <tr>
                <th>
                    <label for="description">Description</label>
                </th>
                <td>
                    <textarea name="description" id="description"></textarea>
                </td>
            </tr>
            <tr>
                <th>
                    <label for="description">Upload an archive</label>
                </th>
                <td>
                    <?php Ckan_Api_Functions::add_template('partials/snippets/image_upload') ?>
                </td>
            </tr>
        </tbody>
    </table>
    <button class="button button-primary add" data-object="documentation">Submit</button>
</div>