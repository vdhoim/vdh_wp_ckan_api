<div class="package add_new_form">
    <h2>Add Dataset</h2>
    <h3>Demo Heading - Placeholder</h3>
    <table class="form-table">
        <tbody>
            <tr>
                <th>
                    <label for="title">Dataset Name</label>
                </th>
                <td>
                    <input id="title" name="title" type="text" />
                    <span class="description">Description</span>
                </td>
            </tr>
            <tr>
                <th>
                    <label for="title">Organization</label>
                </th>
                <td>
                    <?php
                    Ckan_Api_Functions::add_template('partials/snippets/organization_list', array(
                        'organizations' => $data['organizations'],
                        'current_organization' => isset($data['organizations'][0]) ? $data['organizations'][0] : '' 
                    ))
                    ?>
                </td>
            </tr>
            <tr>
                <th>
                    <label for="description">Description</label>
                </th>
                <td>
                    <textarea name="notes" id="description"></textarea>
                </td>
            </tr>

            <tr>
                <th>
                    <label for="status">Data Maturity</label>
                </th>
                <td>
                    <?php
                    Ckan_Api_Functions::add_template('partials/snippets/dataset_data_maturity', array(
                        'data_maturity' => '',
                        'maturity_levels' => $data['data_maturity_levels']))
                    ?>
                </td>
            </tr>

            <tr>
                <th>
                    <label for="status">Data Steward</label>
                </th>
                <td>
                    <?php
                    $current_user = wp_get_current_user();
                    Ckan_Api_Functions::add_template('partials/snippets/combobox', array(
                        'objects' => $data['users'],
                        'objects_name' => 'maintainer',
                        'current_id' => $current_user->user_email
                    ))
                    ?>                </td>
            </tr>

            <tr>
                <th>
                    <label for="status">License</label>
                </th>
                <td>
                    <?php
                    Ckan_Api_Functions::add_template('partials/snippets/license_select', array(
                        'name' => 'license_id',
                        'licenses' => $data['licenses'],
                        'current_license' => isset($data['dataset_default_values']['license_id']) ?
                                $data['dataset_default_values']['license_id'] :
                                ''))
                    ?>              
                </td>
            </tr>

            <tr>
                <th>
                    <label for="status">Version</label>
                </th>
                <td>
                    <input type="text" name="version" value="<?php echo (isset($data['dataset_default_values']['version']) ? $data['dataset_default_values']['version'] : '0.1') ?>" />             
                </td>
            </tr>
            <tr>
                <th>
                    <label for="status">Groups</label>
                </th>
                <td>
                    <?php
                    Ckan_Api_Functions::add_template('partials/snippets/multiple_select', array(
                        'existing_objects' => array(),
                        'objects' => $data['groups'],
                        'objects_name' => 'groups'
                    ))
                    ?>
                </td>
            </tr>
            <tr>
                <th>
                    <label for="status">Tags</label>
                </th>
                <td>
                    <?php
                    Ckan_Api_Functions::add_template('partials/snippets/multiple_select', array(
                        'existing_objects' => array(),
                        'objects' => $data['tags'],
                        'objects_name' => 'tags'
                    ))
                    ?>                
                </td>
            </tr>
            <tr>
                <th>
                    <label for="status">Expert Text</label>
                </th>
                <td>
                    <?php
                    Ckan_Api_Functions::add_template('partials/snippets/expert_text', array(
                        'posts' => $data['posts'],
                        'expert_text' => ''
                    ))
                    ?>
                </td>
            </tr>
            <tr>
                <th>
                    <label for="status">Documentation</label>
                </th>
                <td>
                    <?php
                    Ckan_Api_Functions::add_template('partials/snippets/documentation', array(
                        'existing_documentation' => null,
                        'documentations' => $data['documentations'],
                    ))
                    ?>
                </td>
            </tr>
        </tbody>
    </table>
    <h3>Resources</h3>
    <table class="form-table">
        <tbody>
            <tr>
                <th>
                    <label for="title">Add Resources</label>
                </th>
                <td>
<?php Ckan_Api_Functions::add_template('partials/snippets/file_upload_form') ?>
                    <ol class="filelist" start="1"></ol>
                    <ol class="urllist" start="1"></ol>
                </td>

            </tr>
        </tbody>
    </table>
    <button class="button button-primary add" data-object="package">Submit</button>
</div>
<script type="text/html" id="tmpl-fileinfo">
<?php Ckan_Api_Functions::add_template('partials/snippets/resource'); ?>
</script>
<script type="text/html" id="tmpl-resource_inputs">
    <?php Ckan_Api_Functions::add_template('partials/snippets/resource_inputs', array('resource' => (object) array('id' => '{id}', 'name' => '', 'state' => 'active', 'changed' => 'true'))); ?>
</script>
<script type="text/html" id="tmpl-resource_url">
<?php Ckan_Api_Functions::add_template('partials/snippets/resource_url'); ?>
</script>