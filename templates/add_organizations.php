<div class="add_new_form organization">
    <h2>Add Organization</h2>
    <h3>Demo Heading - Placeholder</h3>
    <table class="form-table">
        <tbody>
            <tr>
                <th>
                    <label for="title">Organization Name</label>
                </th>
                <td>
                    <input id="title" name="title" type="text" />
                    <span class="description">Description</span>
                </td>
            </tr>
            <tr>
                <th>
                    <label for="description">Description</label>
                </th>
                <td>
                    <textarea name="description" id="description"></textarea>
                </td>
            </tr>
            <tr>
                <th>
                    <label for="description">Upload an image</label>
                </th>
                <td>
                    <img src="/img/no_image_url.gif" class="preview_image" />
                    <?php Ckan_Api_Functions::add_template('partials/snippets/image_upload') ?>
                </td>
            </tr>
        </tbody>
    </table>
    <button class="button button-primary add" data-object="organization">Submit</button>
</div>