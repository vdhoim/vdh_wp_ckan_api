<input type="hidden" 
       placeholder="name"
       name="resource[<?php echo $resource->id; ?>][name]" 
       value="<?php echo isset($resource->original_name) ? $resource->original_name : $resource->name ?>"/>

<input type="hidden" 
       name="resource[<?php echo $resource->id; ?>][state]" 
       value="<?php echo $resource->state ?>"/>

<input type="hidden" 
       name="resource[<?php echo $resource->id; ?>][changed]" 
       value="<?php echo isset($resource->changed) ? $resource->changed : 'false' ?>"/>
