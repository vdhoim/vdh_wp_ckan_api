<div class="ui-widget">
    <select name="<?php echo $objects_name; ?>" multiple="multiple" style="display:none">
        <?php foreach ($objects as $object): ?>
            <option value="<?php echo "{$object->get_title()}:{$object->get_id()}" ?>" 
                <?php echo (in_array($object->get_title(), $existing_objects) ? ' selected="selected"' : '') ?>>
                <?php echo $object->get_title() ?>
            </option>
        <?php endforeach; ?>
    </select>
    <input name="new_<?php echo $objects_name; ?>" 
           class="multiselect" size="40" value="<?php echo implode(', ', $existing_objects); ?>">
</div>