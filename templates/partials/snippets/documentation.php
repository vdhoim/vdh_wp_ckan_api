<select name="documentation">
    <option value="">Select value...</option>
    <?php foreach ($documentations as $documentation): ?>
        <option value="<?php echo $documentation->get_name() ?>"
        <?php echo ($existing_documentation === $documentation->get_name() ? 'selected="selected"' : ''); ?>
                ><?php echo $documentation->get_title(); ?></option>
            <?php endforeach; ?>
</select>
<?php if ($existing_documentation): ?>
    <a href="<?php echo(get_site_url() . '/documentation/' . $existing_documentation) ?>" target="_blank">
        see documentation
    </a>
<?php endif; ?>