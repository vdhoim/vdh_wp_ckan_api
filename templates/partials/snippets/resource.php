<li>
    <?php if (isset($resource->id)): ?>
        <?php Ckan_Api_Functions::add_template('partials/snippets/resource_inputs', array('resource' => $resource)); ?>
    <?php endif; ?>
    <span class="mime-icon <?php
    echo (isset($resource->mimetype_inner) ?
            str_replace('.', '-', str_replace('/', '_', $resource->mimetype_inner)) :
            '{mime}' );
    ?>"></span>
    <span class="name"><?php
        //if there's an original name, show it, if not, look for the name and finally show the template variable for js replacement
        echo (isset($resource->original_name) ?
                $resource->original_name : (
                isset($resource->name) ?
                        $resource->name : '{name}' ));
        ?>
    </span>   
    <?php
    //see explanation above
    echo (isset($resource->size) ?
            "({$resource->size})" :
            isset($resource) && property_exists($resource, 'size') && $resource->size === null ? '' :
                    '({size})' );
    ?>
    <span class="remove-file" data-i="<?php echo (isset($i) ? $i : '{i}'); ?>">x</span>
</li>