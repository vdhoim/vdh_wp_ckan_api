<select name="data_maturity">
    <?php foreach($maturity_levels as $maturity_level): ?>
    <option value="<?php echo $maturity_level; ?>" <?php echo ($data_maturity === $maturity_level ? ' selected="selected"' : ''); ?>>
        <?php echo ucfirst($maturity_level); ?>
    </option>
    <?php endforeach; ?>
</select>