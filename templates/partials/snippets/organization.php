<li class="menu-item menu-item-edit-inactive">
    <dl class="menu-item-bar">
        <dt class="menu-item-handle">
        <span class="item-title">
            <span class="menu-item-title">
                <?php echo $organization->get_title(); ?>
            </span>
        </span>
        <span class="item-controls">
            <span class="item-type">
                <?php echo Ckan_Api_Functions::wrap($organization->get_package_count(), 'Datasets: $var') ?>
            </span>
            <a class="item-edit item-open" href="#">#</a>
        </span>
        </dt>
    </dl>

    <div class="menu-item-settings organization" data-id="<?php echo $organization->get_id() ?>" data-changed="false">

        <?php if ($organization->get_id()): ?>

            <input type="hidden" name="state" value="<?php echo $organization->get_state(); ?>">
            <?php Ckan_Api_Functions::add_template('partials/snippets/remove_object', array('object' => 'organization')) ?>

            <?php echo Ckan_Api_Functions::wrap($organization->get_display_name(), '<h4>$var</h4>') ?>
            
            <a href="<?php echo(get_option('ckan_url') . 'organization/' . $organization->get_name()) ?>" target="_blank">
                <img src="<?php echo $organization->get_image_display_url(); ?>" 
                     title="<?php echo $organization->get_display_name(); ?>" 
                     alt="<?php echo $organization->get_display_name(); ?>" 
                     class="preview_image" />
            </a>
            <?php Ckan_Api_Functions::add_template('partials/snippets/image_upload') ?>

            
            <h4>Description</h4>
            <textarea name="description"><?php echo $organization->get_description() ?></textarea>

            <div class="save_wrapper">
                <span>Don't forget to </span>
                <button class="button button-small button-primary add" data-object="organization">Save Organization</button>
            </div>
        <?php else: ?>
            <div class="organization">
                <input type="hidden" name="title" value="<?php echo $organization->get_title(); ?>" />
                <button class="button button-primary add" data-object="organization">
                    Add this department to CKAN organizations
                </button>
            </div>
        <?php endif; ?>
    </div>
</li>
