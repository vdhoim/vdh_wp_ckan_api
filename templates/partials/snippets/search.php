<form class="search_form">
    <input type="hidden" name="page" value="dashboard" />
    <input type="hidden" name="o" value="<?php echo $object ?>" />
    <input type="text" name="q" class="search" />
    <button class="button button-primary search">Search</button>
    <button class="button button-primary reset">Reset</button>
</form>