Choose from existing materials
<br />
<select name="expert_text">
    <option>Choose Material...</option>
    <?php foreach ($posts as $post): ?>
        <option value="<?php echo $post->ID ?>" 
                <?php echo ($expert_text == $post->ID ? ' selected="selected"' : '') ?>>
                    <?php echo $post->post_title ?>
        </option>
    <?php endforeach; ?>
</select>

or <a href="#" class="create_expert_text">Create a new one</a>
<div class="expert_text" style="display:none">
    <h4 style="margin:0">Title</h4>
    <input type="text" />
    <h4 style="margin:0">Text</h4> 
    <textarea></textarea><br />
    <button class="add_expert_text button button-primary button-small">Add</button>
</div>


<?php if(false): ?>
    <?php if ($expert_text): ?>
        <a href="<?php echo get_permalink($expert_text); ?>" target="_blank">see material</a>
    <?php endif; ?>
    <br />
    or <a href="/wp-admin/post-new.php" target="_blank">create a new one</a>
<?php endif; ?>