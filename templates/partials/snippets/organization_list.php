<?php if (is_array($organizations) && count($organizations) > 1): ?>
    <select name="owner_org">
        <?php foreach ($organizations as $organization): ?>
            <option value="<?php echo $organization->get_id() ?>" 
                    <?php echo ($current_organization && $organization->get_id() === $current_organization->get_id() ? 'selected="selected"' : '') ?>>
                        <?php echo $organization->get_title() ?>
            </option>
        <?php endforeach; ?>
    </select>
<?php else: ?>
    <?php if (is_object($current_organization)): ?>
        <?php echo Ckan_Api_Functions::wrap($current_organization->get_title(), '<h4>Organization:</h4><p>$var</p>') ?>
    <?php endif; ?>
<?php endif; ?>




