<li class="menu-item menu-item-edit-inactive">
    <dl class="menu-item-bar">
        <dt class="menu-item-handle">
        <span class="item-title">
            <span class="menu-item-title">
                <?php echo $dataset->get_title(); ?>
            </span>
            <span class="is-submenu">
                <?php echo 'v ' . $dataset->get_version(); ?>
            </span>
        </span>
        <span class="item-controls">
            <span class="item-type" title="<?php echo $dataset->get_author(); ?>">
                by <?php echo $dataset->get_author(true); ?>
            </span>
            <a class="item-edit item-open" href="#">#</a>
        </span>
        </dt>
    </dl>

    <div class="menu-item-settings package" data-id="<?php echo $dataset->get_id() ?>" data-changed="false">

        <input type="hidden" name="state" value="<?php echo $dataset->get_state(); ?>">

        <?php Ckan_Api_Functions::add_template('partials/snippets/remove_object', array('object' => 'package')) ?>
        
        <h4>Description</h4>
        <textarea name="notes"><?php echo $dataset->get_notes('stripslashes'); ?></textarea>

        <h4>Status</h4>
        <?php Ckan_Api_Functions::add_template('partials/snippets/dataset_data_maturity', array('data_maturity' => $dataset->get_data_maturity(), 'maturity_levels' => $maturity_levels)) ?>

        <h4>Organization:</h4>
        <?php Ckan_Api_Functions::add_template('partials/snippets/organization_list', array('organizations' => $organizations, 'current_organization' => $dataset->get_organization())) ?>

        <h4>Data Steward</h4>
        <?php
        Ckan_Api_Functions::add_template('partials/snippets/combobox', array(
            'objects' => $users,
            'objects_name' => 'maintainer',
            'current_id' => $maintainer_email
        ))
        ?>

        <h4>License</h4>
        <?php
        Ckan_Api_Functions::add_template('partials/snippets/license_select', array(
            'name' => 'license_id',
            'licenses' => $licenses,
            'current_license' => $dataset->get_license_id()))
        ?>

        <h4>Version</h4>
        <input type="text" name="version" value="<?php echo $dataset->get_version() ?>" />

        <h4>Groups</h4>
        <?php
        Ckan_Api_Functions::add_template('partials/snippets/multiple_select', array(
            'existing_objects' => $dataset->get_existing_titles('groups'),
            'objects' => $groups,
            'objects_name' => 'groups'
        ))
        ?>

        <h4>Tags</h4>
        <?php
        Ckan_Api_Functions::add_template('partials/snippets/multiple_select', array(
            'existing_objects' => $dataset->get_existing_titles('tags'),
            'objects' => $tags,
            'objects_name' => 'tags'
        ))
        ?>

        <h4>Expert Text</h4>
        <?php Ckan_Api_Functions::add_template('partials/snippets/expert_text', array(
            'posts' => $posts,
            'expert_text' => $dataset->get_expert_text()
        )) ?> 
        
        <h4>Documentation</h4>       
        <?php
        Ckan_Api_Functions::add_template('partials/snippets/documentation', array(
            'existing_documentation' => $dataset->get_documentation(),
            'documentations' => $documentations,
        ))
        ?>

        <h4>Data and Resources:</h4>
        <ol>
            <?php foreach ($dataset->get_resources() as $i => $resource): ?>
                <?php Ckan_Api_Functions::add_template('partials/snippets/resource', array('resource' => $resource, 'i' => $i)) ?>
            <?php endforeach; ?>
        </ol>
        <ol class="filelist" start="<?php echo (isset($i) ? $i + 2 : 1) ?>"></ol>
        <ol class="urllist" start="<?php echo (isset($i) ? $i + 2 : 1) ?>"></ol>
        <?php Ckan_Api_Functions::add_template('partials/snippets/file_upload_form') ?>

        <div class="save_wrapper">
            <span>Don't forget to </span>
            <button class="button button-small button-primary add" data-object="package">Save Dataset</button>
        </div>
    </div>
</li>