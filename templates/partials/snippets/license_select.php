<select name="<?php echo $name; ?>" class="license">
    <?php foreach ($licenses as $license): ?>
        <option value="<?php echo $license->id; ?>" data-info="<?php echo $license->url ?>"
                <?php echo($current_license === $license->id ? ' selected="selected"' : ''); ?>>
                    <?php echo $license->title; ?>
        </option>
    <?php endforeach; ?>
</select>