<li class="menu-item menu-item-edit-inactive">
    <dl class="menu-item-bar">
        <dt class="menu-item-handle">
        <span class="item-title">
            <span class="menu-item-title">
                <?php echo $group->get_title(); ?>
            </span>
        </span>
        <span class="item-controls">
            <span class="item-type">
                <?php echo Ckan_Api_Functions::wrap($group->get_package_count(), 'Datasets: $var') ?>
            </span>
            <a class="item-edit item-open" href="#">#</a>
        </span>
        </dt>
    </dl>

    <div class="menu-item-settings group" data-id="<?php echo $group->get_id() ?>" data-changed="false">

        <input type="hidden" name="state" value="<?php echo $group->get_state(); ?>">
        <?php Ckan_Api_Functions::add_template('partials/snippets/remove_object', array('object' => 'group')) ?>

        <?php echo Ckan_Api_Functions::wrap($group->get_display_name(), '<h4>$var</h4>') ?>

        <a href="<?php echo(get_option('ckan_url') . 'group/' . $group->get_name()) ?>" target="_blank">
            <img src="<?php echo $group->get_image_display_url(); ?>" 
                 title="<?php echo $group->get_display_name(); ?>" 
                 alt="<?php echo $group->get_display_name(); ?>"  
                 style="max-width:190px;max-height:130px;" />
        </a>
        <?php Ckan_Api_Functions::add_template('partials/snippets/image_upload') ?>


        <h4>Description</h4>
        <textarea name="description"><?php echo $group->get_description() ?></textarea>

        <div class="save_wrapper">
            <span>Don't forget to </span>
            <button class="button button-small button-primary add" data-object="group">Save Group</button>
        </div>
    </div>
</li>
