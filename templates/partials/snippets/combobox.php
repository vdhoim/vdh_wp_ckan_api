<div class="ui-widget">
    <select name="<?php echo $objects_name; ?>" class="combobox">
        <option value="">Select one...</option>
        <?php foreach ($objects as $id => $object): ?>
            <option value="<?php echo "$id:$object"; ?>"
                    <?php echo ($current_id === $id ? ' selected="selected"' : '') ?>
                    ><?php echo $object; ?></option>
        <?php endforeach; ?>
    </select>
    <input name="new_<?php echo $objects_name; ?>" type="hidden" class="new_value">
</div>