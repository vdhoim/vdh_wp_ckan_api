<ul class="menu nav-menus-php">
    <?php foreach ($data['groups'] as $group): ?>
        <?php
        Ckan_Api_Functions::add_template('partials/snippets/group', array(
            'group' => $group
        ));
        ?>
    <?php endforeach; ?>
</ul>