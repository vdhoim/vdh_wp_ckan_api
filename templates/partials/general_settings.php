<form method="post" action="options.php">
    <?php settings_fields('ckan_api'); ?>
    <?php do_settings_sections('ckan_api'); ?>
    <table class="form-table">
        <tr valign="top">
            <th scope="row">CKAN Url</th>
            <td><input type="text" name="ckan_url" value="<?php echo $data['ckan_url']; ?>" /></td>
        </tr>

        <tr valign="top">
            <th scope="row">CKAN Sysadmin Api Key</th>
            <td><input type="text" name="ckan_sysadmin_api_key" value="<?php echo $data['ckan_sysadmin_api_key']; ?>" /></td>
        </tr>

        <tr valign="top">
            <th scope="row">Tableau servers white list</th>
            <td>
                <textarea name="tableau_servers_white_list"><?php echo $data['tableau_servers_white_list']; ?></textarea>
                <span class="description">One Tableau server per line</span>
        </tr>

        <tr valign="top">
            <th scope="row">Datasets Per Page</th>
            <td>
                <select name="datasets_per_page">
                    <?php for ($i = 1; $i < 100; $i++): ?>
                        <option value="<?php echo $i ?>"
                        <?php echo ($i == $data['datasets_per_page'] ? 'selected="selected"' : '') ?>
                                ><?php echo $i ?></option>
                            <?php endfor; ?>
                </select>
            </td>
        </tr>

    </table>

    <?php submit_button(); ?>

</form>