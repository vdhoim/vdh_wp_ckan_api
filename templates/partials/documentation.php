<li class="menu-item menu-item-edit-inactive">
    <dl class="menu-item-bar">
        <dt class="menu-item-handle">
        <span class="item-title">
            <span class="menu-item-title">
                <?php echo $documentation->get_title(); ?>
            </span>
        </span>
        <span class="item-controls">
            <span class="item-type">
            </span>
            <a class="item-edit item-open" href="#">#</a>
        </span>
        </dt>
    </dl>
   
    <div class="menu-item-settings documentation" data-id="<?php echo $documentation->get_name() ?>" data-changed="false">

        <input type="hidden" name="state" value="<?php echo $documentation->get_state(); ?>">
        <?php Ckan_Api_Functions::add_template('partials/snippets/remove_object', array('object' => 'documentation')) ?>

        <?php echo Ckan_Api_Functions::wrap($documentation->get_title(), '<h4>$var</h4>') ?>

        <a href="<?php echo(get_site_url() . '/documentation/' . $documentation->get_name()) ?>" target="_blank">
            Visit the documentation page (<?php echo $documentation->get_title(); ?>)
        </a>
        <?php Ckan_Api_Functions::add_template('partials/snippets/image_upload') ?>


        <h4>Description</h4>
        <textarea name="description"><?php echo $documentation->get_description() ?></textarea>

        <div class="save_wrapper">
            <span>Don't forget to </span>
            <button class="button button-small button-primary add" data-object="documentation">Save Documentation</button>
        </div>
    </div>
</li>
