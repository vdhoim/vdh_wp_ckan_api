<ul class="menu nav-menus-php">
    <?php foreach($data['organizations'] as $organization): ?>
        <?php Ckan_Api_Functions::add_template('partials/snippets/organization', array('organization' => $organization)); ?>
    <?php endforeach; ?>
</ul>
<ul class="menu nav-menus-php">
    <?php foreach($data['departments'] as $department): ?>
        <?php Ckan_Api_Functions::add_template('partials/snippets/organization', array('organization' => $department)); ?>
    <?php endforeach; ?>
</ul>