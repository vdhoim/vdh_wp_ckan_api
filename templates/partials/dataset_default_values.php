<form method="post" action="options.php">
    <?php settings_fields('ckan_dataset'); ?>
    <?php do_settings_sections('ckan_dataset_default_values'); ?>


    <?php
    //default values
    $values = array_merge(
            array(
        'state' => 'active',
        'data_maturity' => 'experimental',
        'private' => '1',
        'license_id' => '',
        'version' => '',
        'datasets_per_page' => 10
            ), $data['dataset_default_values']);
    ?> 



    <input type="hidden" name="ckan_dataset_default_values[state]" value="active" />
    <table class="form-table">
        <tr valign="top">
            <th scope="row">Default Data Maturity</th>
            <td>
                <select name="ckan_dataset_default_values[data_maturity]"> 
                    <?php foreach ($data['data_maturity'] as $data_maturity): ?>
                        <option value="<?php echo $data_maturity ?>"<?php echo($values['data_maturity'] === $data_maturity ? ' selected="selected"' : ''); ?>
                                ><?php echo ucfirst($data_maturity); ?></option>
                            <?php endforeach; ?>
                </select>
            </td>
        </tr>
        <tr valign="top">
            <th scope="row">License</th>
            <td>
                <?php
                Ckan_Api_Functions::add_template('partials/snippets/license_select', array(
                    'name' => 'ckan_dataset_default_values[license_id]',
                    'licenses' => $data['licenses'],
                    'current_license' => $values['license_id']))
                ?>
            </td>
        </tr>

        <tr valign="top">
            <th scope="row">Version</th>
            <td><input type="text" name="ckan_dataset_default_values[version]" 
                       value="<?php echo $values['version']; ?>" />
            </td>
        </tr>

    </table>

<?php submit_button(); ?>

</form>