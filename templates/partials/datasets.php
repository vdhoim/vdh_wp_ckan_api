<?php $maturity_levels = Ckan_Dataset::get_dataset_available_maturity_levels(); ?>
<ul class="menu nav-menus-php">
    <?php foreach ($data['datasets'] as $dataset): ?>
        <?php
        Ckan_Api_Functions::add_template(
                'partials/snippets/dataset', array(
            'dataset' => $dataset,
            'groups' => $data['groups'],
            'organizations' => $data['organizations'],
            'licenses' => $data['licenses'],
            'maturity_levels' => $maturity_levels,
            'users' => $data['users'],
            'tags' => $data['tags'],
            'posts' => $data['posts'],
            'documentations' => $data['documentations'],
            'maintainer_email' => $dataset->get_maintainer_email() ? $dataset->get_maintainer_email() : $dataset->get_author_email()
        ));
        ?>
    <?php endforeach; ?>
</ul>

