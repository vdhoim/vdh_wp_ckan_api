<?php if ($data->error): ?>
    <h2><?php echo ($data->error->__type ? $data->error->__type : 'Operational Error'); ?></h2>



    <h4>The issues are: </h4>
    <ol>
        <?php foreach ($data->error as $field => $problem): ?>
            <?php if (preg_match('/^\_/', $field)): ?>
                <?php continue; ?>
            <?php else: ?>
                <li>
                    <?php echo $field . ': ' . (is_array($problem) ? implode(' ', $problem) : $problem); ?>
                </li>
            <?php endif; ?>

        <?php endforeach; ?>
    </ol>
<?php endif; ?>
    
    
<small>Help: <?php echo ($data->help ? $data->help : 'Please report this problem'); ?></small>