<h2>Documentation</h2>
<h3>The documentation should be zipped into .zip archive and the archive root should contain index.html file</h3>
<button class="button button-primary add_new documentation">
    Add New Documentation
</button>
<div>
    <?php if (isset($documentations) && is_array($documentations) && count($documentations)): ?>
        <ul class="menu nav-menus-php">
            <?php foreach ($documentations as $documentation): ?>
                <?php
                Ckan_Api_Functions::add_template("partials/documentation", array(
                    'documentation' => $documentation));
                ?>
            <?php endforeach; ?>
        </ul>
    <?php else: ?>
        <span>No documentations found</span>
    <?php endif; ?>
</div>
<button class="button button-primary add_new documentation">
    Add New Documentation
</button>
<?php Ckan_Api_Functions::add_template("add_documentation"); ?>
<div class="overlay"></div>