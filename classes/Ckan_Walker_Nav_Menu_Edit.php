<?php

/**
 * Here we will modify our WP Admin menu view depending on variables received 
 * from CKAN (if any)
 */
class Ckan_Walker_Nav_Menu_Edit extends Walker_Nav_Menu {

    /**
     * Starts the list before the elements are added.
     *
     * @see Walker_Nav_Menu::start_lvl()
     *
     * @since 3.0.0
     *
     * @param string $output Passed by reference.
     * @param int    $depth  Depth of menu item. Used for padding.
     * @param array  $args   Not used.
     */
    public function start_lvl(&$output, $depth = 0, $args = array()) {
        
    }

    /**
     * Ends the list of after the elements are added.
     *
     * @see Walker_Nav_Menu::end_lvl()
     *
     * @since 3.0.0
     *
     * @param string $output Passed by reference.
     * @param int    $depth  Depth of menu item. Used for padding.
     * @param array  $args   Not used.
     */
    public function end_lvl(&$output, $depth = 0, $args = array()) {
        
    }

    /**
     * Start the element output.
     *
     * @see Walker_Nav_Menu::start_el()
     * @since 3.0.0
     *
     * @param string $output Passed by reference. Used to append additional content.
     * @param object $item   Menu item data object.
     * @param int    $depth  Depth of menu item. Used for padding.
     * @param array  $args   Not used.
     * @param int    $id     Not used.
     */
    public function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {
        global $_wp_nav_menu_max_depth;
        $_wp_nav_menu_max_depth = $depth > $_wp_nav_menu_max_depth ? $depth : $_wp_nav_menu_max_depth;

        ob_start();
        $item_id = esc_attr($item->ID);
        $removed_args = array(
            'action',
            'customlink-tab',
            'edit-menu-item',
            'menu-item',
            'page-tab',
            '_wpnonce',
        );

        $original_title = '';
        if ('taxonomy' == $item->type) {
            $original_title = get_term_field('name', $item->object_id, $item->object, 'raw');
            if (is_wp_error($original_title))
                $original_title = false;
        } elseif ('post_type' == $item->type) {
            $original_object = get_post($item->object_id);
            $original_title = get_the_title($original_object->ID);
        }

        $classes = array(
            'menu-item menu-item-depth-' . $depth,
            'menu-item-' . esc_attr($item->object),
            'menu-item-edit-' . ( ( isset($_GET['edit-menu-item']) && $item_id == $_GET['edit-menu-item'] ) ? 'active' : 'inactive'),
        );

        $title = $item->title;

        if (!empty($item->_invalid)) {
            $classes[] = 'menu-item-invalid';
            /* translators: %s: title of menu item which is invalid */
            $title = sprintf(__('%s (Invalid)'), $item->title);
        } elseif (isset($item->post_status) && 'draft' == $item->post_status) {
            $classes[] = 'pending';
            /* translators: %s: title of menu item in draft status */
            $title = sprintf(__('%s (Pending)'), $item->title);
        }

        $title = (!isset($item->label) || '' == $item->label ) ? $title : $item->label;

        $submenu_text = '';
        if (0 == $depth)
            $submenu_text = 'style="display: none;"';

        $ckan_url = get_option('ckan_url');

        Ckan_Api_Functions::add_template('partials/menu-item', get_defined_vars());

        if ($item->url === $ckan_url) {
            $this->_add_ckan_tree($ckan_url, get_defined_vars());
        }

        $output .= ob_get_clean();
    }

    private function _add_ckan_tree($ckan_url, $vars) {
        function _render_menu_tree($arr, $vars, $level=1){
            foreach ($arr as $el) {
            $vars['item_id'] = $el['id'];
            $vars['item']->title = $vars['title'] = $el['name'];
            $vars['item']->url = $el['url'];
            $vars['classes'] = array(
                'menu-item menu-item-depth-' . $level,
                'menu-item-' . esc_attr($vars['item']->object),
                'menu-item-edit-inactive',
            );
            $vars['item']->type_label = 'CKAN Automatically Generated';
            $vars['item']->readonly = true;
            Ckan_Api_Functions::add_template('partials/menu-item', $vars);
            if(isset($el['children']) && $el['children']){
                _render_menu_tree($el['children'], $vars, $level+1);
            }
        }
        }
        $ckan_api = new Ckan_Api($ckan_url);
        $ckan_header = $ckan_api->get_header_menu($vars['item']->ID);
        _render_menu_tree($ckan_header, $vars);
    }
}