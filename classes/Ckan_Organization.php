<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Ckan_Organization
 *
 * @author asiaba
 */
class Ckan_Organization extends Ckan_Base {

    protected $_fields = array(
        'name' => '',
        'display_name' => '',
        'title' => '',
        'description' => '',
        'image_url' => '',
        'image_display_url' => '',
        'package_count' => '',
    );

    public function get_image_display_url() {
        $img = $this->_fields['image_display_url'];
        if (!$img) {
            $img = '/img/no_image_url.gif';
        }
        return $img;
    }

    public function save() {
        if (!function_exists('wp_handle_upload')) {
            require_once( ABSPATH . 'wp-admin/includes/file.php' );
        }
        $image = wp_handle_upload($_FILES['image'], array('test_form' => false));
        if ($image && !isset($image['error']) && preg_match('/^image\/.*$/', $image['type'])) {
            $this->set_image_url($image['url']);
            $this->set_image_display_url($image['url']);
        }
        if(!get_term_by('name', $this->get_title(), 'category')){
            wp_insert_term($this->get_title(), 'category', array('parent' => 0));
        }
        return array('result' => true, 'response' => parent::save());
    }

}
