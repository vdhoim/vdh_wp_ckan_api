<?php

class Ckan_Helper {

    public static function get_departments_difference(array $ckan_organizations) {
        $departments = array();
        foreach (get_categories(array('hide_empty' => false)) as $d) {
            if ($d->category_parent === '0') {
                $departments[$d->name] = $d;
            }
        }
        foreach ($ckan_organizations as $ckan_organization) {
            if (isset($departments[$ckan_organization->get_title()])) {
                unset($departments[$ckan_organization->get_title()]);
            }
        }
        foreach ($departments as $d) {
            $departments[$d->name] = new Ckan_Organization(array(
                'name' => $d->name,
                'title' => $d->name,
                'description' => $d->description
            ));
        }
        return $departments;
    }
}
