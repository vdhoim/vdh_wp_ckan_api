<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Ckan_Resource
 *
 * @author asiaba
 */
class Ckan_Resource extends Ckan_Base {

    protected $_fields = array(
        'original_name' => '',
        'name' => '',
        'package_id' => '',
        'url' => '',
        'revision_id' => '',
        'description' => '',
        'format' => '',
        'hash' => '',
        'resource_type' => '',
        'mimetype' => '',
        'mimetype_inner' => '',
        'webstore_url' => '',
        'cache_url' => '',
        'size' => '',
        'created' => '',
        'last_modified' => '',
        'cache_last_updated' => '',
        'webstore_last_updated' => '',
        'upload' => '',
    );

    protected function _has_file() {
        return true;
    }
    
    public static function save_resources(array $resources, $package_id = null, $need_rearrangement = true) {
        $res_array = $need_rearrangement ?
                self::rearrange_resources($resources) :
                $resources;
        foreach ($res_array as $resource) {
            $r = new self($resource);
            $r->set_package_id($package_id);
            $r->save();
        }
    }

    public static function update_resources(array $resources, $package_id = null) {
        foreach ($resources as $id => $resource) {
            if (isset($resource['url']) && filter_var($resource['url'], FILTER_VALIDATE_URL)) {
                self::save_url_resource($resource, $package_id);
            } elseif ($resource['changed'] === 'true') {
                $resource['id'] = $id;
                $r = new self($resource);
                $r->save();
            }
        }
    }

    public static function save_url_resource(array $resource, $package_id) {
        $resources = array(
            array(
                'url' => $resource['url'],
                'name' => $resource['name'],
                'format' => 'URL',
                'mimetype' => 'url',
        ));
        self::save_resources($resources, $package_id, false);
    }

    private static function rearrange_resources(array $resources) {
        $res = array();
        foreach ($resources['name'] as $i => $r) {
            $res[] = array(
                'name' => $r,
                'mimetype_inner' => $resources['type'][$i],
                'upload' => $resources['tmp_name'][$i],
                'size' => $resources['size'][$i],
                'url' => ''
            );
        }
        return $res;
    }

    protected function _validate_name($name) {
        $n = explode('.', $name);
        if (count($n) < 2) {
            $n[] = 'ckan';
        }
        $this->set_original_name($name);
        $this->set_format($n[count($n) - 1]);
        $this->set_mimetype($n[count($n) - 1]);

        return implode(' ', array_map('ucfirst', array_slice($n, 0, -1)));
    }

    protected function _validate_format($format) {
        return strtoupper($format);
    }
    
    protected function _validate_url($url){
        if($url){
            foreach(explode("\n", get_option('tableau_servers_white_list')) as $tableau_server){
                if(strpos(trim($url), trim($tableau_server)) === 0){
                    $this->set_resource_type('tableau');
                    $this->set_mimetype_inner('application/twb');
                }
            }
        }
        return $url;
    }

}
