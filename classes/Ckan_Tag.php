<?php

class Ckan_Tag extends Ckan_Base {

    protected $_fields = array(
        'display_name' => '',
        'name' => '',
        'title' => '',
        'scalar' => ''
    );

    protected function _get_list_action() {
        return 'tag_list?all_fields=true';
    }

    public function save() {
        $this->set_id($this->get_name());
    }

    /*
      private function _create_vocabulary($name) {
      return $this->_adapter->send_request('vocabulary_create', array('name' => $name));
      }

      private function _get_vocabulary_id($name) {
      $v = $this->_adapter->send_request("vocabulary_show?id=$name");
      if (is_array($v) &&
      isset($v['error']) &&
      is_object($v['error']) &&
      isset($v['error']->__type) &&
      $v['error']->__type === 'Not Found Error') {
      $v = $this->_create_vocabulary(
      $this->get_vocabulary_id());
      }
      return $v->id;
      }
     */

    protected function _validate_title($title) {
        $this->set_name($title);
        return $title;
    }

    protected function _validate_name($name) {
        return $this->get_name() ? $this->get_name() : $name;
    }

    public function get_title() {
        return $this->_fields['name'] ? $this->_fields['name'] : $this->_fields['scalar'];
    }

}
