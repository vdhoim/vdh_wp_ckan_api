<?php

class Ckan_Base {

    protected $_id;
    protected $_adapter;
    protected $_fields = array();
    protected $_default_fields = array(
        'id' => '',
        'state' => 'active'
    );

    public function __construct($fields = array()) {
        $this->_adapter = new Ckan_Api();
        $this->_fields = array_merge($this->_default_fields, $this->_fields);

        if (count((array) $fields) < count($this->_fields) && is_array($fields) && isset($fields['id']) && $fields['id']) {
            $obj_fields = (array) $this->get_by_id($fields['id']);
            $fields = array_merge($obj_fields, (array) $fields);
        }
        foreach ((array) $fields as $k => $v) {
            if (isset($this->_fields[$k])) {
                $this->{'set_' . $k}($v);
            }
        }
        if (!$this->get_name() && $this->get_title()) {
            $this->set_name($this->get_title());
        }
    }

    public function get_id() {
        return isset($this->_id) ? $this->_id : $this->_fields['id'];
    }

    public function set_id($id) {
        $this->_fields['id'] = $id;
        $this->_id = $id;
    }

    protected function _get_save_action() {
        return strtolower(implode('_', array_slice(explode('_', get_class($this), 2), 1))) . ($this->_id ? ($this->get_state() === 'deleted' ? '_delete' : '_update') : '_create');
    }

    protected function _has_file() {
        return false;
    }

    public function __call($name, $arguments) {
        $params = explode('_', $name, 2);
        $action = $this->_getter_or_setter_or_action($params[0]);
        if (isset($params[1])) {
            if ($action) {
                return $this->{'_' . $action}($params[1], $arguments);
            } else {
                return strtolower(implode('_', array_slice(explode('_', get_class($this), 2), 1))) .
                        preg_replace('/^(get|set)([a-z\_]+)_action$/', '$2', $params[1]);
            }
        }
        throw new Exception('The field or method does not exist');
    }

    private function _getter_or_setter_or_action($action) {
        return preg_match('/^(get|set)$/', $action) ? $action : '';
    }

    private function _get($field, array $callbacks) {
        $value = null;
        if (isset($this->_fields[$field])) {
            $value = $this->_fields[$field];
            foreach ($callbacks as $callback) {
                $value = $callback($value);
            }
        }
        return $value;
    }

    private function _set($field, array $values) {
        foreach ($values as $value) {
            if(isset($this->_fields[$field])){
                $this->_fields[$field] = $this->_validate($field, $value);
            }
        }
        return true;
    }

    private function _validate($field, $value) {
        $object = $this->_fields[$field];
        switch (true) {
            case $object && is_object($value):
                lazy_include($object);
                $value = new $object((array) $value);
                break;

            case $object && is_array($value) && $value:
                lazy_include($object[0]);
                foreach ($value as $key => $obj) {
                    $value[$key] = new $object[0]((array) $obj);
                }
                break;
        }
        return (method_exists($this, '_validate_' . $field) ?
                        call_user_func_array(array($this, '_validate_' . $field), array($value)) :
                        $value);
    }

    protected function _clean_from_classname_values() {
        foreach ($this->_fields as $field => $v) {
            if (is_array($v) && count($v) === 1 && isset($v[0]) && is_string($v[0]) && preg_match('/^Ckan\_[A-z]+$/', $v[0])) {
                $this->{'set_' . $field}(array());
            } elseif (is_string($v) && preg_match('/^Ckan\_[A-z]+$/', $v)) {
                $this->{'set_' . $field}('');
            }
        }
    }

    protected function _to_array() {
        $this->_fields['id'] = $this->_id;
        return $this->_fields;
    }

    public static function get_instance() {
        return new static;
    }

    public function save() {
        $response = $this->_adapter->send_request(
                $this->_get_save_action(), $this->_to_array(), $this->_has_file());
        if (isset($response->id)) {
            $this->_id = $response->id;
        }
        return $response;
    }

    public function get_list($get_action = '', $offset = 0, $limit = null) {
        if ($limit === null) {
            $limit = (int) get_option('datasets_per_page');
        }
        if (!$get_action) {
            $get_action = $this->_get_list_action();
        }
        $objects = array();
        foreach ($this->get_list_raw($get_action, $offset, $limit) as $object) {
            $obj = new static($object);
            $objects[] = $obj;
        }
        return $objects;
    }

    public function get_list_raw($get_action, $offset, $limit) {
        return $this->_adapter->send_request($get_action . "?offset=$offset&limit=$limit");
    }

    public function get_by_id($id) {
        return $this->_adapter->send_request($this->_get_show_action(), array('id' => $id));
    }

    protected function _validate_name($title) {
        return strtolower(preg_replace('/[^A-Za-z0-9\-]+/', '-', $title));
    }

}
