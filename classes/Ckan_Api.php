<?php

class Ckan_Api {

    private $_api_key;
    private $_ckan_url;

    public function __construct($url = null) {
        $this->_ckan_url = $url ? $url : get_option('ckan_url');
        $this->_api_key = wp_get_current_user()->ckan_api_key;
        if (!$this->_api_key && current_user_can('administrator')) {
            $this->_api_key = get_option('ckan_sysadmin_api_key');
        }
    }

    /**
     * 
     * @param WP_User $u
     * 
     * return string Ckan user api key
     */
    public function create_user(WP_User $u) {
        lazy_include('Ckan_User');
        $user = new Ckan_User($u);
        $user->save();
        return $user->apikey;
    }

    /**
     * 
     * @param type $element_id
     * @return type
     */
    public function get_header_menu($element_id) {
        $ckan_header = array();
        foreach (array('vdh_package_list', 'organization_list', 'group_list') as $action) {
            $ckan_header[] = $this->_get_menu_block($action);
        }
        return $this->_add_ids($ckan_header, $element_id);
    }

    /**
     * 
     * @return string
     */
    public function get_url() {
        return $this->_url;
    }

    private function _add_ids($arr, $p_id) {
        $id = $p_id * 100;
        foreach ($arr as $k => $v) {
            $arr[$k]['id'] = --$id;
            $arr[$k]['parent'] = $p_id;
            if (isset($v['children']) && $v['children']) {
                $arr[$k]['children'] = $this->_add_ids($v['children'], $id);
            }
        }
        return $arr;
    }

    /**
     * 
     * @param type $block
     * @return type
     */
    private function _get_menu_block($block) {

        $block_mapping = array(
            'vdh_package_list' => 'datasets',
            'current_package_list_with_resources' => 'datasets',
            'organization_list' => 'organizations',
            'group_list' => 'groups',
        );
        try{
            $c = $this->send_request($block);
        } catch(Exception $e){
            $c = array();
        }
        $children = $this->_get_menu_children($c, $block_mapping[$block]);

        return $this->_build_menu_item_array(ucfirst($block_mapping[$block]), '#', $children);
    }

    /**
     * 
     * @param type $name
     * @param type $url
     * @param type $children
     * @return type
     */
    private function _build_menu_item_array($name, $url, $children) {
        return array(
            'id' => '',
            'name' => $name,
            'url' => $url,
            'parent' => '',
            'children' => $children
        );
    }

    /**
     * 
     * @param type $children
     * @param type $parent
     * @return type
     */
    private function _get_menu_children($children, $parent) {
        $block_children = array();
        foreach ($children as $child) {
            $url = $this->_ckan_url . substr($parent, 0, -1) . "/$child";
            $name = implode(' ', array_map('ucfirst', explode('-', $child)));
            $block_children[] = $this->_build_menu_item_array($name, $url, array());
        }
        return $block_children;
    }

    /**
     * 
     * @param type $action
     * @param type $data
     * @return type
     */
    public function send_request($action, $data = array(), $multipart = false) {
        lazy_include('Ckan_Api_Request');
        $request = Ckan_Api_Request::get_instance(
                        $this->_api_key, $this->_get_ckan_endpoint($action));
        if ($data) {
            $request->set_data($data, $multipart);
        }
        return $request->send();
    }

    /**
     * 
     * @param array $data
     * @return type
     */
    private function _set_headers(array $data, $multipart) {
        $date = new DateTime(NULL, new DateTimeZone('UTC'));
        $headers = array(
            'Date: ' . $date->format('D, d M Y H:i:s') . ' GMT', // RFC 1123
            'Accept: application/json;q=1.0, application/xml;q=0.5, */*;q=0.0',
            'Accept-Charset: utf-8',
            'Accept-Encoding: gzip',
        );
        if ($this->_api_key) {
            $headers[] = "Authorization: {$this->_api_key}";
        }
        if ($data && !$multipart) {
            $headers[] = "Content-Type: application/json";
        }
        if ($multipart) {
            $this->_boundary = $this->_ckan_client . substr(str_shuffle(sha1(rand() . time())), 0, 15);
            $headers[] = "Content-Type: multipart/form-data;boundary={$this->_boundary}";
            $headers[] = "Cache-Control: no-cache";
        }
        return $headers;
    }

    private function _get_ckan_endpoint($action, array $data = array()) {
        return $this->_ckan_url . "api/3/action/" . $action;
    }

}
