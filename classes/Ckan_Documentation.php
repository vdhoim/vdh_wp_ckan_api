<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Ckan_Documentation
 *
 * @author asiaba
 */
class Ckan_Documentation extends Ckan_Base {

    protected $_fields = array(
        'name' => '',
        'title' => '',
        'description' => ''
    );

    public function __construct($fields = array()) {
        if(isset($fields['id']) && $fields['id']){
            $documentations = get_option('documentation');
            if(isset($documentations[$fields['id']])){
                $fields = array_merge($documentations[$fields['id']], $fields);
            }
            $fields['id'] = '';
        }
        parent::__construct($fields);
        $this->set_name($this->get_title());
    }

    public function save() {
        $archive = isset($_FILES['image']) ? $_FILES['image'] : null;
        $doc_path = ABSPATH . 'wp-content/documentation/';
        if(!file_exists($doc_path) && !mkdir($doc_path, 0755)){
            return array('error' => true, 'msg' => 'Check creating folder priviledges');
        }
        if (isset($archive['type']) && preg_match('/^application\/.*zip$/', $archive['type'])) {
            if (file_exists($doc_path) || mkdir($doc_path, 0777)) {
                $this->_save_archive($doc_path, $archive);
            }
        }
        $documentation = get_option('documentation');
        if (!$documentation) {
            $documentation = array();
        }
        $documentation[$this->get_name()] = $this->_to_array();
        if ($this->get_state() === 'deleted' ) {
            unset($documentation[$this->get_name()]);
            system('rm -rf ' . escapeshellarg($doc_path . $this->get_name()));
            
        }
        update_option('documentation', $documentation);
        return array('name' => $this->get_name());
    }

    private function _save_archive($doc_path, $archive) {
        $zip = new ZipArchive;
        if ($zip->open($archive['tmp_name']) && $zip->locateName('index.html')) {
            $single_doc_path = $doc_path . $this->get_name();
            system('rm -rf ' . escapeshellarg($single_doc_path));
            mkdir($single_doc_path, 0755);
            $zip->extractTo($single_doc_path);
            $zip->close();
        }
    }

    public static function get_documentations() {
        $documentations = get_option('documentation') ? get_option('documentation') : array();
        foreach($documentations as $name => $documentation){
            $documentations[$name] = new self($documentation);
        }
        return $documentations;
    }

}
