<?php

class Ckan_Api_Request {

    private $_curl;
    private $_multipart;
    private $_api_key;
    private $_ckan_client;
    private $_body;
    private $_output;
    private $_error_msg;
    private $_emergency_data = array();

    public function __construct($api_key = null, $url = null) {
        if (!function_exists('curl_version')) {
            die('Curl must be enabled on the server');
        }
        $this->_curl = curl_init();
        $this->set_api_key($api_key);
        $this->set_url($url);
        $this->_ckan_client = 'CkanClientPHP';
        curl_setopt_array($this->_curl, array(
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_MAXREDIRS => 5,
            CURLOPT_AUTOREFERER => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_USERAGENT => $this->_set_user_agent(),
            CURLINFO_HEADER_OUT => true,
            CURLOPT_FILETIME => true,
        ));
    }

    public function __destruct() {
        if (isset($this->_curl) && is_resource($this->_curl)) {
            curl_close($this->_curl);
            unset($this->_curl);
        }
    }

    public function set_url($url) {
        $this->_emergency_data['url'] = $url;
        curl_setopt($this->_curl, CURLOPT_URL, $url);
    }

    public function set_api_key($api_key) {
        $this->_api_key = $api_key;
    }

    private function _recursively_remove_objects($arr) {
        foreach ($arr as $k => $val) {
            if (is_object($val) && !$val instanceof stdClass) {
                $arr[$k] = array('id' => $val->get_id());
            }
            if (is_array($val)) {
                $arr[$k] = $this->_recursively_remove_objects($val);
            }
        }
        return $arr;
    }

    public function set_data(array $data = array(), $multipart = false) {
        $data = $this->_recursively_remove_objects($data);
        $this->_body = json_encode($data);
        if ($multipart) {
            $this->_body = $this->_generate_multipart_body($data);
            curl_setopt($this->_curl, CURLOPT_POST, 1);
        }
        curl_setopt($this->_curl, CURLOPT_POSTFIELDS, $this->_body);
        return $this;
    }

    public function send() {
        $this->_set_headers();
        $this->_output = json_decode(
                curl_exec($this->_curl));
        $this->_error_msg = curl_error($this->_curl);
        curl_close($this->_curl);
        unset($this->_curl);
        return $this->_get_result();
    }

    private function _get_result() {
        return $this->_is_legitimate() ?
                $this->_get_result_or_conflict() :
                $this->_get_error();
    }

    private function _is_legitimate() {
        return is_object($this->_output) && $this->_error_msg === '';
    }

    private function _get_result_or_conflict() {
        return $this->_output->success ?
                $this->_output->result :
                array('error' => $this->_output->error);
    }

    private function _get_error() {
        if ($this->_error_msg) {
            if (preg_match('/^Connection\stimed\sout/', $this->_error_msg)) {
                throw new Exception($this->_error_msg, 1);
            }
        } elseif (is_string($this->_output)) {
            $error = $this->_output . ' Url: ' . $this->_emergency_data['url'];
            throw new Exception($error, 0);
        }
    }

    private function _generate_multipart_body($data) {
        $this->_multipart = true;
        $this->_boundary = $this->_ckan_client . substr(str_shuffle(sha1(rand() . time())), 0, 15);
        $body = '';
        foreach ($data as $field => $value) {
            if (!$value && $field !== 'url') {
                continue;
            }
            $body .= "\n--{$this->_boundary}\n";
            $body .= "Content-Disposition: form-data; name=\"$field\"";
            if ($field === 'upload') {
                $body .= "; filename=\"{$data['original_name']}\"\n";
                $body .= "Content-Type: {$data['mimetype_inner']}\n";
                $body .= file_get_contents($value);
            } else {
                $body .= "\n\n$value";
            }
        }
        $body .= "\n--{$this->_boundary}--";
        return $body;
    }

    /**
     * 
     * @param array $data
     * @return type
     */
    private function _set_headers() {
        $date = new DateTime(NULL, new DateTimeZone('UTC'));
        $headers = array(
            'Date: ' . $date->format('D, d M Y H:i:s') . ' GMT', // RFC 1123
            'Accept: application/json;q=1.0, application/xml;q=0.5, */*;q=0.0',
            'Accept-Charset: utf-8',
            'Accept-Encoding: gzip',
        );
        if ($this->_api_key) {
            $headers[] = "Authorization: {$this->_api_key}";
        }
        if ($this->_multipart) {
            $headers[] = "Content-Type: multipart/form-data;boundary={$this->_boundary}";
            $headers[] = "Cache-Control: no-cache";
        } elseif ($this->_body) {
            $headers[] = "Content-Type: application/json";
        }
        curl_setopt($this->_curl, CURLOPT_HTTPHEADER, $headers);
    }

    /**
     * 
     * @return type
     */
    private function _set_user_agent() {
        $server_name = '';
        if (@$_SERVER['SERVER_PORT'] === '80') {
            $server_name = 'http://' . $_SERVER['SERVER_NAME'];
        }
        return "{$this->_ckan_client} ($server_name{$_SERVER['PHP_SELF']})";
    }

    /**
     * 
     * @return \self
     */
    public static function get_instance($api_key = null, $url = null) {
        return new self($api_key, $url);
    }

}
