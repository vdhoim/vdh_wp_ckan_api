<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Ckan_User
 *
 * @author asiaba
 */
class Ckan_User extends Ckan_Base {

    protected $_fields = array(
        'name' => '',
        'password' => '',
        'email' => '',
        'fullname' => '',
        'about' => '',
        'apikey' => ''
    );

    public function __construct($user) {

        if ($user instanceof WP_User) {
            $user = array(
                'name' => $user->user_nicename,
                'password' => substr(str_shuffle(strtolower(sha1(rand() . time() . "randomize_password"))), 0, 10),
                'email' => $user->user_email,
                'fullname' => $user->user_login,
                'about' => ""
            );
        }
        parent::__construct($user);
    }

    public function save() {
        $r = parent::save();
        if (isset($r->success) && $r->success === false) {
            $user = $this->_find_user($this->get_name());
            if ($user) {
                return $user->save();
            }
        }
        return $r;
    }

    private function _find_user($name) {
        $user = $this->_adapter->send_request($this->_get_list_action() . "?q=$name");
        if ($user && isset($user[0]) && $user[0]->id) {
            $user = (array) $user[0];
            foreach (array_keys($user) as $key) {
                if (in_array($key, array('about', 'fullname'))) {
                    $user[$key] = $this->{'get_' . $key}();
                }
            }
            return new self($user);
        }
    }

}
