<?php

class Ckan_Dataset extends Ckan_Base {

    protected $_fields = array(
        'name' => '',
        'title' => '',
        'author' => '',
        'author_email' => '',
        'maintainer' => '',
        'maintainer_email' => '',
        'license_id' => '',
        'notes' => '',
        'url' => '',
        'version' => '',
        'state' => '',
        'type' => '',
        'tags' => array('Ckan_Tag'),
        'extras' => array(),
        'organization' => 'Ckan_Organization',
        'owner_org' => '',
        'groups' => array('Ckan_Group'),
        'resources' => array(),
        'resource' => array(),
        'private' => true,
        'data_maturity' => 'experimental',
        'documentation' => '',
        'expert_text' => ''
    );
    private static $_data_maturity_levels = array(
        'author' => array(
            'experimental',
            'provisional'
        ),
        'editor' => array(
            'internal'
        ),
        'administrator' => array(
            'experimental',
            'internal',
            'public'
        )
    );

    public function __construct($fields = array()) {
        $fields = (array) $fields;
        $fields['groups'] = $this->_add_create_objects($fields, 'groups');
        $fields['tags'] = $this->_add_create_objects($fields, 'tags');
        parent::__construct($fields);
    }

    private function _keyize($array) {
        $return_array = array();
        foreach ($array as $element) {
            if ($element && $element !== 'null') {
                $el = explode(':', $element);
                $return_array[$el[0]] = (isset($el[1]) ? $el[1] : '');
            }
        }
        return $return_array;
    }

    private function _add_create_objects($post, $objects) {
        $return_value = array();
        if (isset($post[$objects]) && $post[$objects] && is_array($post[$objects])) {
            $return_value = $post[$objects];
        }
        if (isset($post["new_$objects"]) && $post["new_$objects"]) {
            $obj = 'Ckan_' . ucfirst(substr($objects, 0, -1));
            lazy_include($obj);
            $current_objects = $this->_keyize(array_map('trim', explode(',', $post[$objects])));
            $new_objects = isset($post["new_$objects"]) && $post["new_$objects"] ?
                    array_map('trim', explode(',', $post["new_$objects"])) :
                    array();
            foreach ($new_objects as $new_object) {
                if (!in_array($new_object, array_keys($current_objects)) && $new_object) {
                    $o = new $obj(array('title' => $new_object));
                    $o->save();
                    $current_objects[$o->get_title()] = $o->get_id();
                }
            }
            foreach ($current_objects as $current_object_name => $current_object_id) {
                $index = $objects === 'tags' ? 'name' : 'id';
                $return_value[] = array(
                    $index => $objects === 'tags' ? $current_object_name : $current_object_id
                );
            }
        }
        return $return_value;
    }

    protected function _get_save_action() {
        return $this->_id ? 'package_update' : 'package_create';
    }

    protected function _get_list_action() {
        return 'current_package_list_with_resources';
    }

    protected function _get_show_action() {
        return 'package_show';
    }

    public function get_author($strip_length = false) {
        $author = $this->_fields['author'] ?
                $this->_fields['author'] :
                $this->_fields['maintainer'];
        $max_length = ckan_api_settings('max_author_name_length') ?
                ckan_api_settings('max_author_name_length') :
                20;
        if ($strip_length && strlen($author) > $max_length) {
            $author = substr($author, 0, $max_length) . '...';
        }
        return $author;
    }

    public function save() {
        $response = $this->_run_validation();
        if (!$response) {
            $response = parent::save();
        }
        $this->_save_resources();
        return array('result' => true, 'response' => $response);
    }

    public function get_search_list($query) {
        $results = $this->_adapter->send_request('package_search', array(
            'q' => $query
        ));
        $objects = array();
        foreach ($results->results as $object) {
            $objects[] = new self($object);
        }
        return $objects;
    }

    private function _save_resources() {
        if ($this->_id) {
            if ($_FILES && isset($_FILES['resources']) && is_array($_FILES['resources'])) {
                lazy_include('Ckan_Resource');
                Ckan_Resource::save_resources($_FILES['resources'], $this->_id);
            }
            if ($this->get_resource()) {
                lazy_include('Ckan_Resource');
                Ckan_Resource::update_resources($this->get_resource(), $this->_id);
            }
        }
    }

    private function _run_validation() {
        if (!$this->get_owner_org()) {
            $this->_force_assign_organization();
        }
        $this->_clean_from_classname_values();
        $this->_simplify_tags();
        if (!$this->_id) {
            $default_values = get_option('ckan_dataset_default_values');
            $user = wp_get_current_user();
            $this->set_url(get_option('ckan_url') . 'dataset/' . $this->get_name() . '/');
            if (!$this->_fields['author']) {
                $this->set_author($user->display_name);
                $this->set_author_email($user->user_email);
            }
            if (!$this->get_maintainer()) {
                $this->set_maintainer($user->display_name);
                $this->set_maintainer_email($user->user_email);
            }
            $this->set_private(true); //one more time make sure if everything is nice and private
            foreach ($default_values as $field => $default_value) {
                if (!$this->{'get_' . $field}() || $field === 'private') {
                    $this->{'set_' . $field}($default_value);
                }
            }
            if ($this->get_resource()) {
                $resources = $this->get_resource();
                $this->set_resource('');
                $response = parent::save();
                $this->set_resource($resources);
                return $response;
            }
        }
        if($this->get_expert_text()){
            wp_publish_post($this->get_expert_text());
        }
        return false;
    }
    
    private function _simplify_tags(){
        $tags = $this->get_tags();
        foreach($tags as $key => $tag){
            $tags[$key] = $tag->_to_array();
        }
        $this->_fields['tags'] = $tags;
    }

    private function _force_assign_organization() {
        if($this->get_data_maturity() === 'public'){
            return;
        }
        lazy_include('Ckan_Organization');
        $organizations = Ckan_Organization::get_instance()->get_list('organization_list_for_user', 0, 1);
        if ($organizations) {
            $this->set_owner_org($organizations[0]->get_id());
        } else {
            throw new Exception('The organization should be assigned to the dataset, unless the dataset is public');
        }
    }

    protected function _validate_private($value) {
        return (bool) $value;
    }

    protected function _validate_resources(array $resources) {
        return $resources;
    }

    protected function _validate_author($author) {
        return $this->_fields['author'] ? $this->_fields['author'] : $author;
    }

    protected function _validate_author_email($author_email) {
        return $this->get_author_email() ? $this->get_author_email() : $author_email;
    }

    protected function _validate_maintainer($maintainer) {
        $m = explode(':', $maintainer);
        if (count($m) > 1) {
            $maintainer = $m[1];
            $this->set_maintainer_email($m[0]);
        }
        return $maintainer;
    }

    protected function _validate_maintainer_email($email) {
        return $this->get_maintainer_email() ? $this->get_maintainer_email() : $email;
    }

    /*
      protected function _validate_data_maturity($data_maturity) {
      if (in_array($data_maturity, self::get_dataset_available_maturity_levels())) {
      return $data_maturity;
      }
      return $this->get_data_maturity();
      }
     * */

    public static function get_dataset_available_maturity_levels($data_maturity = null) {
        global $current_user;

        $data_maturity_levels = (array) $data_maturity;
        foreach ($current_user->roles as $role) {
            if (isset(self::$_data_maturity_levels[$role])) {
                $data_maturity_levels = array_merge($data_maturity_levels, self::$_data_maturity_levels[$role]);
            }
        }
        return array_unique($data_maturity_levels);
    }

    public function get_existing_titles($objects) {
        $return_array = array();
        foreach ($this->{"get_$objects"}() as $object) {
            $return_array[] = $object->get_title();
        }
        return $return_array;
    }

}
