<?php

class Ckan_Api_Controller {

    private $_user;

    public function __construct() {
        $this->_user = wp_get_current_user();
    }

    private function _get_users() {
        $users = array();
        foreach (get_users() as $user) {
            $users[$user->user_email] = $user->display_name;
        }
        return $users;
    }

    private function _get_search_modifiers() {
        $items_per_page = (int) get_option('datasets_per_page');
        $modifiers = array(
            'datasets_offset' => 0,
            'organizations_offset' => 0,
            'groups_offset' => 0,
            'datasets_query' => '',
            'organizations_query' => '',
            'groups_query' => '',
        );

        $object = isset($_REQUEST['o']) ? $_REQUEST['o'] : '';
        $page = isset($_REQUEST['p']) && (int) $_REQUEST['p'] > 0 ? $_REQUEST['p'] : '';
        $query = isset($_REQUEST['q']) ? $_REQUEST['q'] : '';

        if ($object && $page) {
            $modifiers[$object . '_offset'] = (int) $page * $items_per_page;
        }
        if ($object && $query) {
            $modifiers[$object . '_query'] = $query;
        }
        return $modifiers;
    }

    public function documentation_action() {
        lazy_include('Ckan_Documentation');
        $this->_render_view('documentation', array(
            'documentations' => Ckan_Documentation::get_documentations()));
    }

    public function dashboard_action() {
        lazy_include(array('Ckan_Dataset', 'Ckan_Organization', 'Ckan_Group', 'Ckan_Tag', 'Ckan_Documentation'));

        extract($this->_get_search_modifiers());

        if ($datasets_query) {
            $datasets = Ckan_Dataset::get_instance()->get_search_list($datasets_query);
            $datasets_list = $datasets;
        } else {
            $datasets = Ckan_Dataset::get_instance()->get_list('', $datasets_offset);
            $datasets_list = Ckan_Dataset::get_instance()->get_list_raw('vdh_package_list', 0, 0);
        }
        $organizations = Ckan_Organization::get_instance()->get_list('organization_list_for_user');
        $groups = Ckan_Group::get_instance()->get_list('group_list_authz');
        $users = $this->_get_users();
        $tags = Ckan_Tag::get_instance()->get_list();
        $documentations = Ckan_Documentation::get_documentations();
        $posts = get_posts(array('posts_per_page' => 99999));
        $data = array(
            'datasets' => array(
                'active' => !isset($_REQUEST['tab']) || isset($_REQUEST['tab']) && $_REQUEST['tab'] === 'Datasets',
                'datasets' => $datasets,
                'datasets_list' => $datasets_list,
                'organizations' => $organizations,
                'licenses' => Ckan_Dataset::get_instance()->get_list_raw('license_list', 0, 0),
                'groups' => $groups,
                'users' => $users,
                'tags' => $tags,
                'posts' => $posts,
                'data_maturity_levels' => Ckan_Dataset::get_dataset_available_maturity_levels(),
                'documentations' => $documentations,
                'dataset_default_values' => (array) get_option('ckan_dataset_default_values')
            ),
            'organizations' => array(
                'active' => isset($_REQUEST['tab']) && $_REQUEST['tab'] === 'Organizations',
                'organizations' => $organizations,
                'departments' => Ckan_Helper::get_departments_difference($organizations),
            //'organizations_list' => Ckan_Dataset::get_instance()->get_list_raw('organization_list', 0, 0),
            ),
            'groups' => array(
                'active' => isset($_REQUEST['tab']) && $_REQUEST['tab'] === 'Groups',
                'groups' => $groups,
            //'groups_list' => Ckan_Dataset::get_instance()->get_list_raw('group_list', 0, 0),
            ),
        );
        $this->_render_view('dashboard', array('data' => $data));
    }

    public function settings_action() {
        lazy_include('Ckan_Dataset');
        $data = $this->_get_general_settings();
        if (get_option('ckan_url')) {
            $data = array_merge(array(
                'dataset_default_values' => array(
                    'dataset_default_values' => (array) get_option('ckan_dataset_default_values'),
                    'licenses' => Ckan_Dataset::get_instance()->get_list_raw('license_list', 0, 0),
                    'data_maturity' => Ckan_Dataset::get_dataset_available_maturity_levels()
                )), $data);
            //$data['syncronize_with_CKAN'] = array();
        }
        $this->_render_view('settings', array('data' => $data));
    }

    public function save_ajax() {
        try {
            $object = 'Ckan_' . ucfirst($_POST['object']);
            lazy_include($object);
            $o = new $object($_POST);
            $this->_send_json($o->save());
        } catch (Exception $e) {
            $this->_send_json(array('error' => array('Integration Error' => $e->getMessage())));
        }
    }

    public function expert_text_ajax() {
        if (!(current_user_can('publish_posts') && current_user_can('publish_pages'))) {
            $this->_send_json(array('result' => false, 'msg' => 'You do not have sufficient rights'));
        }
        if (!(isset($_POST['title']) && isset($_POST['text']) && isset($_POST['category']))) {
            $this->_send_json(array('result' => false, 'msg' => 'Not enough data to complete the request'));
        }
        $category = get_term_by('name', $_POST['category'], 'category');
        if (is_object($category) && $category->term_id) {
            $post_id = wp_insert_post(array(
                'post_title' => $_POST['title'],
                'post_content' => $_POST['text'],
                'post_category' => array($category->term_id)
            ));

            if ($post_id) {
                $this->_send_json(array('result' => true, 'id' => $post_id));
            }
            $this->_send_json(array('result' => false, 'msg' => 'Error on saving the post'));
        }
        $this->_send_json(array('result' => false, 'msg' => 'Category Not Found'));
    }

    private function _get_general_settings() {
        return array(
            'general_settings' => array(
                'ckan_url' => get_option('ckan_url'),
                'ckan_sysadmin_api_key' => get_option('ckan_sysadmin_api_key'),
                'tableau_servers_white_list' => get_option('tableau_servers_white_list'),
                'datasets_per_page' => get_option('datasets_per_page')),
        );
    }

    /**
     * Extracts passed data and renders the view.
     *
     * @param string $view 
     * @param array $data - once extracted, you can use the array key's as standard PHP variables in your view
     */
    private function _render_view($view, $data = null) {
        if (!empty($data)) {
            extract($data);
        }
        include CKAN_API_BASEPATH . "/templates/$view.php";
    }

    private function _send_json($arr = array()) {
        header('Content-Type: application/json');
        echo json_encode($arr);
        exit;
    }

    public function render_api_error(Exception $e) {
        switch ($e->getCode()) {
            case 1:
                $view = 'settings';
                $data = array(
                    'data' => $this->_get_general_settings(),
                    'error' => array(
                        'msg' => $e->getMessage(),
                        'tab' => 'general_settings'
                    )
                );
                break;
            default:
                $view = 'error';
                $data = array();
        }

        $this->_render_view($view, $data);
        exit;
    }

}
