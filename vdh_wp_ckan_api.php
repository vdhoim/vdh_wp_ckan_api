<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('error_reporting', E_ALL);


/*
  Plugin Name: CKAN API
  Plugin URI: http://vdh.virginia.gov
  Description:
  Version: 0.1
  Author: Alex Siaba
  Author Email: alex.siaba@vdh.virginia.gov
  License:

  Copyright 2014 Alex Siaba (alex.siaba@gmail.com)

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License, version 2, as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

 */


/*
  FUNCTIONS - overridden function from the wp-core
 */

define('CKAN_API_BASEPATH', dirname(__FILE__));

//possibly will be included into plugin setup
function ckan_api_settings($setting) {
    $settings = array(
        'max_author_name_length' => 20,
    );
    if (isset($settings[$setting])) {
        return $settings[$setting];
    }
    return '';
}

function lazy_include($class) {
    if (is_array($class)) {
        foreach ($class as $instance) {
            lazy_include($instance);
        }
    }
    if (is_string($class) && !class_exists($class)) {
        $filepath = CKAN_API_BASEPATH . "/classes/$class.php";
        if (file_exists($filepath)) {
            include_once $filepath;
        } else {
            throw new Exception("The API Object Class $class not Found!");
        }
    }
}

lazy_include(array('Ckan_Api', 'Ckan_Api_Controller', 'Ckan_Base', 'Ckan_Helper'));

require_once 'functions.php';
require_once 'router.php';
