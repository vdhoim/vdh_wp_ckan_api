<?php

class Ckan_Api_Functions {

    static function get_instance() {
        return new self;
    }

    /**
     * adding actions and hooks
     */
    public function add_actions() {

        add_action('admin_init', array($this, 'register_settings'));
        add_filter('wp_edit_nav_menu_walker', array($this, 'walker_class_name'));
        add_action('admin_enqueue_scripts', array($this, 'add_static_files'));
        add_action('admin_menu', array($this, 'extend_admin_menu'));
        add_action('after_setup_theme', array($this, 'ajax_route'));
        add_filter('pre_update_option_ckan_url', array($this, 'check_ckan_url'));
        add_filter('pre_update_option_tableau_servers_white_list', array($this, 'serialize_tableau_servers'));
        add_filter('option_tableau_servers_white_list', array($this, 'unserialize_tableau_servers'));

        //binding wp user with ckan user and saving CKAN API key to wp user meta field       
        add_action('edit_user_profile_update', array($this, 'bind_user_to_ckan'));
        add_action('user_register', array($this, 'bind_user_to_ckan'));

        //sending post to Solr
        add_action('save_post', array($this, 'solr_update_post'));
        add_action('delete_post', array($this, 'solr_delete_post'));
        
        //custom search form
        add_action('get_search_form', array($this, 'ckan_search_form'));
        

        if (!get_option('datasets_per_page')) {
            update_option('datasets_per_page', 10);
        }

        if (is_multisite()) {
            add_filter("network_admin_plugin_action_links_" . plugin_basename(__DIR__ . '/ckan_api.php'), array($this, 'ckan_api_settings_link'));
        } else {
            add_filter("plugin_action_links_" . plugin_basename(__DIR__ . '/ckan_api.php'), array($this, 'ckan_api_settings_link'));
        }

        add_shortcode('documentation', array($this, 'route_shortcode'));
    }
    
    public function ckan_search_form(){
        $this->add_template('search_form', array('search_url' => get_option('ckan_url') . 'dataset'));
        return '';
    }

    public function solr_update_post($post_id) {        
        if (array_diff((array) wp_get_post_categories($post_id), (array) get_option('exclude_categories')) && get_post_status($post_id) === 'publish') {
            lazy_include('Ckan_Api');
            $adapter = new Ckan_Api;
            $post = get_post($post_id);
            if(!$post->post_excerpt){
                $post_lines = explode("\n", $post->post_content);
                $post->post_excerpt = wp_strip_all_tags(trim($post_lines[0]));
            }
            $adapter->send_request('wp_post_add', array(
                'id'    => $post_id,
                'title' => $post->post_title,
                'url'   => get_permalink($post_id),
                'text'  => wp_strip_all_tags($post->post_content),
                'notes' => $post->post_excerpt
            ));
        } else {
            $this->solr_delete_post($post_id);
        }
    }

    public function solr_delete_post($post_id) {
        lazy_include('Ckan_Api');
        $adapter = new Ckan_Api;
        $adapter->send_request('wp_post_remove', array(
            'id' => $post_id
        ));
    }

    public function route_shortcode($atts) {
        $params = shortcode_atts(array(
            'id' => '0',
            'url' => ''
                ), $atts);

        return $params['id'] . ' ' . $params['url'];
    }

    public function ckan_api_settings_link($links) {
        $settings_link = '<a href="admin.php?page=settings">' . __('Settings', 'ckan_api') . '</a>';
        array_unshift($links, $settings_link);
        return $links;
    }

    /**
     * Check if user has ckan api key and if not, register user with CKAN and 
     * save CKAN api key to the wp db
     * 
     * @TODO Could be also encrypted/decrypted to get more security as the api key gets saved into the wp db
     * 
     * @param int $user_id
     */
    public function bind_user_to_ckan($user_id) {
        if (!get_user_meta('ckan_api_key', $user_id)) {
            $user = get_userdata($user_id);
            $ckan_api = new Ckan_Api();
            $ckan_api_key = $ckan_api->create_user($user);
            update_user_meta($user_id, 'ckan_api_key', $ckan_api_key);
        }
    }

    public function register_settings() {
        register_setting('ckan_api', 'ckan_url');
        register_setting('ckan_api', 'ckan_sysadmin_api_key');
        register_setting('ckan_api', 'tableau_servers_white_list');
        register_setting('ckan_api', 'datasets_per_page');
        register_setting('ckan_dataset', 'ckan_dataset_default_values');
        register_setting('documentation', 'documentation');
    }

    public function extend_admin_menu() {
        add_menu_page('CKAN Management', 'CKAN Management', 'ckan_management', 'ckan_management', array($this, 'route'), '', 3);
        add_submenu_page('ckan_management', 'CKAN Dashboard', 'CKAN Dashboard', 'manage_options', 'dashboard', array($this, 'route'));
        add_submenu_page('ckan_management', 'Documentation', 'Documentation', 'manage_options', 'documentation', array($this, 'route'));
        add_submenu_page('ckan_management', 'Settings', 'Settings', 'manage_options', 'settings', array($this, 'route'));
    }

    public function walker_class_name() {
        $cls = 'Ckan_Walker_Nav_Menu_Edit';
        if (!class_exists($cls)) {
            include_once "classes/$cls.php";
        }
        return $cls;
    }

    public function add_static_files($hook) {
        wp_enqueue_script('jquery-ui-autocomplete');
        if ($hook === 'nav-menus.php') {
            wp_enqueue_script('vdh_admin_js', plugin_dir_url(__FILE__) . 'static/admin.js');
        }
        if (preg_match('/^ckan-/', $hook)) {
            wp_enqueue_style('vdh_admin_css', plugin_dir_url(__FILE__) . 'static/admin.css');
            wp_enqueue_script('jquery-ui-tooltip');
            wp_enqueue_script('jquery-ui-button');
            wp_enqueue_script('vdh_admin_js', plugin_dir_url(__FILE__) . 'static/admin.js');
        }

        return;
    }

    public function route() {
        $page = isset($_REQUEST['page']) ? $_REQUEST['page'] : null;
        if ($page) {
            $router = new Ckan_Api_Router($page);
            $router->route();
        }
    }

    public function ajax_route() {
        $request = isset($_REQUEST['q']) ? substr($_REQUEST['q'], 1) : trim($_SERVER['REQUEST_URI'], '/');
        if ($request && preg_match('/^ckan_api\/\w+/', $request)) {
            $request = preg_replace('/^ckan_api\//', '', $request);
            $router = new Ckan_Api_Router($request, true);
            $router->route();
        }
    }

    public static function add_template($template, array $vars = array()) {
        extract($vars);
        include 'templates/' . $template . '.php';
    }

    public static function wrap($variable, $template) {
        $string = '';
        if ($variable) {
            $string = preg_replace('/\$var/', $variable, $template);
        }
        return $string;
    }

    public function check_ckan_url($url) {
        if ($url[strlen($url) - 1] !== '/') {
            $url .= '/';
        }
        $ckan_api = new Ckan_Api($url);
        if ($ckan_api->send_request('site_read') === true) {
            $theme_options = get_option('vdh_theme_options');
            $theme_options['ckan_url'] = $url;
            update_option('vdh_theme_options', $theme_options);
            return $url;
        }
        return '';
    }

    public function serialize_tableau_servers($s) {
        $servers = explode("\n", $s);
        foreach ($servers as $key => $server) {
            $server = $this->_validate_url(trim($server));
            if ($server) {
                $servers[$key] = $server;
            } else {
                unset($servers[$key]);
            }
        }
        return serialize(array_unique($servers));
    }

    private function _validate_url($url) {
        if (!preg_match('/^http(s)*\:\/\/.+/', $url)) {
            $url = "http://$url";
        }
        if (!preg_match('/^http\:\/\/.+\/$/', $url)) {
            $url .= '/';
        }

        return filter_var($url, FILTER_VALIDATE_URL);
    }

    public function unserialize_tableau_servers($s) {
        $servers = unserialize($s);
        if ($servers) {
            return implode("\n", $servers);
        }
        return '';
    }

}

Ckan_Api_Functions::get_instance()->add_actions();
