<?php

class Ckan_Api_Router {

    private $_controller;
    private $_controller_action;
    private $_controller_args;

    public function __construct($page, $ajax = false) {       
        if (!get_option('ckan_url')) {
            $options = get_option('vdh_theme_options');
            if ($options && isset($options['ckan_url']) && $options['ckan_url']) {
                update_option('ckan_url', $options['ckan_url']);
            } else {
                $page = 'settings';
                $this->_controller_args = array(
                    'tab' => 'general_settings',
                    'msg' => 'Need valid CKAN Url'
                );
            }
        }
        $this->_controller = new Ckan_Api_Controller;
        $this->_controller_action = strtolower($page) . ($ajax ? '_ajax' : '_action');
    }

    public function route() {
        if ($this->_is_valid_request()) {
            $this->_send_response();
        }
        return;
    }

    private function _is_valid_request() {
        return method_exists($this->_controller, $this->_controller_action);
    }

    private function _send_response() {
        try {
            $this->_controller->{$this->_controller_action}($this->_controller_args);
        } catch (Exception $e) {
            //pre_dump($e);
            $this->_controller->render_api_error($e);
        }
    }

}
