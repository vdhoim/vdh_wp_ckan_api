(function($) {
    if (!Array.prototype.last) {
        Array.prototype.last = function() {
            return this[this.length - 1];
        };
    }
    if (!Array.prototype.getUnique) {
        Array.prototype.getUnique = function() {
            var u = {}, a = [];
            for (var i = 0, l = this.length; i < l; ++i) {
                if (u.hasOwnProperty(this[i])) {
                    continue;
                }
                a.push(this[i]);
                u[this[i]] = 1;
            }
            return a;
        };
    }

    $(document).ready(function() {
        var ckanLinkHelpers = $('.noAction');
        if (ckanLinkHelpers.length && ckanLinkHelpers[1]) {
            $(ckanLinkHelpers[0]).css('float', 'right').insertAfter($('#custom-menu-item-url'));
            $(ckanLinkHelpers[1]).css({'float': 'right', 'margin': '0 0 0 10px'}).insertAfter($('#custom-menu-item-url'));
        }
        $('.noAction').on('click', function(e) {
            e.preventDefault();
            $(this).siblings('input').val($(this).data('link') || '#');
        });
        $('.nav-tab-wrapper>a').on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();
            $('.tab-pane').removeClass('active').hide();
            $($(this).attr('href')).addClass('active');
        });
        $('.item-edit.item-open').on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();
            $(this).closest('.menu-item-bar').siblings('.menu-item-settings')
                    .slideToggle()
                    .parent('.menu-item').toggleClass('menu-item-edit-active', 'menu-item-edit-inactive');
        });
        $('.create_expert_text').on('click', function(e) {
            e.preventDefault();
            $(this).siblings('.expert_text').slideToggle();
        });
        $('.add_expert_text').on('click', function(e) {
            e.preventDefault();
            var $this = $(this);
            $.post('/ckan_api/expert_text', {
                title: $this.siblings('input').val(),
                text: $this.siblings('textarea').val(),
                category: $.trim($this
                        .closest('.package')
                        .find('select[name="owner_org"]>option:selected')
                        .text())
            }, function(data) {
                if (data.hasOwnProperty('result') && data.result) {
                    $this.parent()
                            .siblings('select[name="expert_text"]')
                            .append($('<option>')
                                    .text($this.siblings('input').val())
                                    .attr('value', data.id)
                                    .prop('selected', true));
                    $this.siblings('input').val('');
                    $this.siblings('textarea').val('');
                } else {
                    alert(data.hasOwnProperty('msg') && !data.result ?
                            data.msg : 'An error occured while saving the post');
                }

            });
        });
        $('button.add').on('click', function(e) {
            toggleSpinner();
            e.stopPropagation();
            e.preventDefault();
            var object = $(this).data('object');
            var formData = new FormData();
            var scope = $(this).closest('.' + object);
            var files = scope.find('.files').length ? scope.find('.files')[0].files : [];
            var image = scope.find('.upload_image').length ? scope.find('.upload_image')[0].files[0] : null;
            formData.append('id', scope.data('id') || '');
            scope.find('input:not([type="file"]), textarea, select').each(function() {
                formData.append($(this).attr('name'), $(this).val());
            });
            $.each(files, function(i, f) {
                !f.removed && formData.append('resources[]', f, $.trim(f.changed_name || f.name));
            });
            image && formData.append('image', image, $.trim(image.name));
            formData.append('object', object === 'package' ? 'dataset' : object);
            $.ajax({
                url: '/ckan_api/save',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success: function(data) {
                    toggleSpinner(true);
                    if (data.error) {
                        var error = '';
                        for (var i in data.error) {
                            error += data.error[i] + '\n';
                        }
                        alert(error);
                    }
                    scope.find('li.deleted').fadeOut('slow', function() {
                        $(this).remove();
                    });
                    toggleSaveButton(scope.find('.save_wrapper'), true);
                    if (data.hasOwnProperty('result') && data.result) {
                        toggleOverlay($('.add_new_form'), true, true);
                    }
                }
            });
        });
        $('.upload_image').on('change', function() {
            if (this.files && this.files[0] && this.files[0].type.substr(0, 6) === 'image/') {
                var reader = new FileReader();
                var $this = $(this);
                reader.onload = function(e) {
                    $this.parent()
                            .find('img.preview_image')
                            .attr('src', e.target.result);
                };
                reader.readAsDataURL(this.files[0]);
            }

        });
        $('.remove').on('click', function(e) {
            e.preventDefault();
            var object = $(this).data('object');
            if (confirm('Do you really want to remove ' + object)) {
                var scope = $(this).closest('.' + object);
                scope.find('input[name="state"]').val('deleted');
                scope.find('button.add').click();
            }
        });
        $('.add_links').on('click', function(e) {
            e.preventDefault();
            var scope = $(this).closest('.package');
            scope.find('.urllist')
                    .append($('<li />')
                            .append($($('#tmpl-resource_inputs')
                                    .html()
                                    .replace('"hidden"', '"text"')
                                    .replace(/\{id\}/g, scope.find('.urllist>li').length)
                                    )).append($(
                            $('#tmpl-resource_url').html().replace('{i}', scope.find('.urllist>li').length)
                            )));

        });
        $('.search_form button.reset').on('click', function(e) {
            e.preventDefault();
            window.location.href = window.location.protocol + '//' + window.location.host + window.location.pathname + '?page=dashboard';
        });
        $('.files').on('change', function() {
            var scope = $(this).closest('.package');
            scope.find('.filelist').html('');
            $.each($(this)[0].files, function(i, f) {
                scope.find('.filelist').append($('#tmpl-fileinfo')
                        .html()
                        .replace('{i}', i)
                        .replace('{mime}', f.type.replace("/", "_").replace(".", "-"))
                        .replace('{name}', f.name)
                        .replace('{size}', function() {
                            return f.size / 1024 > 1 ? (f.size / 1024).toFixed(2) + 'kb' : f.size + 'b';
                        })
                        .replace('{inputs}', ''));
            });
            toggleSaveButton(scope.find('.save_wrapper'));
            scope.find('.urllist').attr('start', $(this)[0].files.length + parseInt(scope.find('.filelist').attr('start')));
        });
        $('.package').on('click', '.remove-file', function() {
            var scope = $(this).closest('.package');
            $(this).siblings('[name*="changed"]').attr('value', true);
            toggleSaveButton(scope.find('.save_wrapper'));
            if ($(this).html() === 'x') {
                $(this).siblings('input[type="hidden"]').length ?
                        $(this).siblings('[name*="state"]').val('deleted') : (
                        scope.find('.files')[0].files[$(this).data('i')].removed = true);
                $(this).html('restore');
                $(this).closest('li').addClass('deleted');
            } else {
                $(this).siblings('input[type="hidden"]').length ?
                        $(this).siblings('[name*="state"]').val('active') : (
                        scope.find('.files')[0].files[$(this).data('i')].removed = false);
                $(this).html('x');
                $(this).closest('li').removeClass('deleted');
            }
        });
        $('.package').on('click', '.name', function() {
            var scope = $(this).closest('.package');
            $(this).siblings('[name*="changed"]').attr('value', true);
            toggleSaveButton(scope.find('.save_wrapper'));
            if (!$(this).siblings('.filename').length) {
                var val = $.trim($(this).text()).split('.');
                var input = $('<input type="text"/>')
                        .addClass('filename')
                        .val(val.length > 1 ? val.slice(0, -1).join('.') : val)
                        .insertBefore($(this));
                $(this).text((val.length > 1 ? '.' : '') + val.slice(1).toString());
                input.on('propertychange change click input paste focus', function() {
                    $(this).css('width', function() {
                        var width = $('<p/>').addClass('measurer').html($(this).val().replace(/\s/g, '&nbsp;')).appendTo('body').width();
                        $('.measurer').remove();
                        return width;
                    });
                });
                input.on('focusout', function() {
                    var name = $(this).val() + $(this).siblings('.name').text();
                    $(this).siblings('.name').text(name);
                    $(this).siblings('input[type="hidden"]').length ?
                            $(this).siblings('[name*="name"]').val($.trim(name)) : (
                            scope.find('.files')[0].files[$(this).siblings('.remove-file').data('i')]['changed_name'] = $.trim(name));
                    $(this).remove();
                });
                input.focus();
            }
        });
        $('.menu-item-settings').on('change', 'input, select, textarea', function() {
            toggleSaveButton($(this).closest('.menu-item-settings').find('.save_wrapper'));
        });
        $('.add_new').on('click', function() {
            toggleOverlay($(this).siblings('.add_new_form'));
        });
        $('.overlay').on('click', function() {
            toggleOverlay($('.add_new_form:visible'), true);
        });
        $('.license').on('change', showLicenseInfo);
        function toggleOverlay(obj, close, reload) {
            if (close) {
                $('.overlay').hide().css('opacity', 0);
                obj.css('opacity', 0).hide(400, function() {
                    if (reload) {
                        var s = '?';
                        var search = window.location.search.substring(1).split('&');
                        for (var i = 0; i < search.length; i++) {
                            var q = search[i].split('=');
                            if (q[0] !== 'tab') {
                                s += search[i];
                            }
                        }
                        var tab = $.trim($('.nav-tab-active').text());
                        window.location.href = '{host}{search}&tab={tab}'
                                .replace('{host}', window.location.href.split('?')[0])
                                .replace('{search}', s)
                                .replace('{tab}', tab);
                    }
                });
            } else {
                $('.overlay').show().animate({'opacity': 0.7});
                obj.show().animate({'opacity': 1});
            }
        }
        function toggleSpinner(hide) {
            hide ? $('.overlay').show().css('z-index', 99999) :
                    $('.overlay').show().css('z-index', 999999);

        }
        function toggleSaveButton(obj, close) {
            !(obj.height() && !close) && obj.animate({height: close ? 0 : 30});
        }
        function showLicenseInfo() {
            $('#licenses').siblings('div').remove();
            $('#licenses>option:selected').data('info') && $('<div/>').append(
                    $('<a/>').attr({href: $('#licenses>option:selected').data('info'), target: '_blank'})
                    .text('About ' + $('#licenses>option:selected').text().trim())
                    ).insertAfter($('#licenses'));
        }
        showLicenseInfo();
        //combobox starts here
        $.widget('custom.combobox', {
            _create: function() {
                this.wrapper = $("<span>")
                        .addClass("custom-combobox")
                        .insertAfter(this.element);

                this.element.hide();
                this._createAutocomplete();
                this._createShowAllButton();
            },
            _createAutocomplete: function() {
                var selected = this.element.children(":selected"),
                        value = selected.val() ? selected.text() : "";

                this.input = $("<input>")
                        .appendTo(this.wrapper)
                        .val(value)
                        .attr("title", "")
                        .addClass("custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left")
                        .autocomplete({
                            delay: 0,
                            minLength: 0,
                            source: $.proxy(this, "_source")
                        })
                        .tooltip({
                            tooltipClass: "ui-state-highlight"
                        });

                this._on(this.input, {
                    autocompleteselect: function(event, ui) {
                        ui.item.option.selected = true;
                        this._trigger("select", event, {
                            item: ui.item.option
                        });
                    },
                    autocompletechange: "_removeIfInvalid"
                });
            },
            _createShowAllButton: function() {
                var input = this.input,
                        wasOpen = false;

                $("<a>").attr("tabIndex", -1)
                        .attr("title", "Show All Items")
                        .tooltip()
                        .appendTo(this.wrapper)
                        .button({
                            icons: {
                                primary: "ui-icon-triangle-1-s"
                            },
                            text: false
                        })
                        .removeClass("ui-corner-all")
                        .addClass("custom-combobox-toggle ui-corner-right")
                        .mousedown(function() {
                            wasOpen = input.autocomplete("widget").is(":visible");
                        })
                        .click(function() {
                            input.focus();
                            if (wasOpen) {
                                return;
                            }
                            input.autocomplete("search", "");
                        });
            },
            _source: function(request, response) {
                var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                response(this.element.children("option").map(function() {
                    var text = $(this).text();
                    if (this.value && (!request.term || matcher.test(text)))
                        return {
                            label: text,
                            value: text,
                            option: this
                        };
                }));
            },
            _removeIfInvalid: function(event, ui) {
                this.input.parents().siblings('input.new_value').val('');
                if (ui.item) {
                    return;
                }
                var value = this.input.val(),
                        valueLowerCase = value.toLowerCase(),
                        valid = false;
                this.element.children("option").each(function() {
                    if ($(this).text().toLowerCase() === valueLowerCase) {
                        this.selected = valid = true;
                        return false;
                    }
                });
                if (valid) {
                    return;
                }
                this.input.parents().siblings('input.new_value').val(value);
            },
            _destroy: function() {
                this.wrapper.remove();
                this.element.show();
            }
        });
        $(".combobox").combobox();

        // multiselect start
        function getValues(obj) {
            var values = [];
            $(obj).siblings('select').find('option').each(function() {
                values.push($.trim($(this).text()));
            });
            return values;
        }
        function split(val) {
            return val.split(/,\s*/);
        }
        function extractLast(term) {
            return split(term).pop();
        }
        $(".multiselect")
                .bind("keydown", function(e) {
                    e.keyCode === $.ui.keyCode.TAB &&
                            $(this).autocomplete("instance").menu.active &&
                            e.preventDefault();
                })
                .autocomplete({
                    minLength: 0,
                    source: function(request, response) {
                        response($.ui.autocomplete.filter(
                                getValues(this.element[0]), extractLast(request.term)));
                    },
                    focus: function() {
                        return false;
                    },
                    select: function(e, ui) {
                        var terms = split(this.value);
                        terms.pop();
                        terms.push(ui.item.value);
                        terms.push("");
                        this.value = terms.getUnique().join(", ");
                        return false;
                    },
                    change: function() {
                        var terms = split(this.value).getUnique();
                        $(this).siblings('select').find('option').each(function() {
                            $(this).removeAttr('selected');
                            if (terms.indexOf($.trim($(this).text())) > -1) {
                                $(this).attr('selected', 'selected');
                            }
                        });
                    }
                });

    });
})(jQuery);